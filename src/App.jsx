import './App.css'
import NavBar from './components/NavBar/NavBar'
import HeroSection from './components/HeroSection/HeroSection'
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom'
import BoardDetails from './components/BoardDetails/BoardDetails'
import 'bootstrap/dist/css/bootstrap.min.css'

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route exact path='/' element={
            <>
              <NavBar />
              <HeroSection />
            </>
          } />
          <Route exact path='/:id' element={
            <>
              <NavBar />
              <BoardDetails />
            </>
          } />
        </Routes>
      </Router>
    </div>
  )
}
export default App