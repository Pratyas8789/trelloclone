import React, { useState } from 'react'
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

export default function AddCheckList(props) {
  const {handleCard, handleToogle}=props
  const [name,setName]=useState("")

  const handleName = (event) => {
    setName(event.target.value)
  }


  return (
    <div
      className="modal show"
      style={{ display: 'block', position: 'initial' }}
    >
      <Modal.Dialog>
      <Modal.Header >
          <Modal.Title>Add checklist</Modal.Title>
          <Modal.Title onClick={handleToogle}><i class="fa fa-times" aria-hidden="true"></i></Modal.Title>

        </Modal.Header>
        <Modal.Body>
          <h5>Title</h5>
          <input onChange={handleName} type="text" name="boardTitle" id="title" />
        </Modal.Body>
        <Modal.Footer className='d-flex justify-content-start'>
          <Button  onClick={() => handleCard(name)} type='submit' variant="primary">Add</Button>
        </Modal.Footer>
      </Modal.Dialog>
    </div>
  )
}
