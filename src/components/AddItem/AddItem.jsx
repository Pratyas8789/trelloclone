import React,{useState} from 'react'
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';


export default function AddItem(props) {
    const {handleItemName, handletoogle}=props
  const [itemName,setItemName]=useState("")

  const handleName = (event) => {
    setItemName(event.target.value)
  }

  return (
    <div
      className="modal show"
      style={{ display: 'block', position: 'initial' }}
    >
      <Modal.Dialog>
        <Modal.Body>
          <input onChange={handleName} type="text" name="boardTitle" id="title" />
        </Modal.Body>
        <Modal.Footer className='d-flex justify-content-between'>
          <Button onClick={() => handleItemName(itemName)} type='submit' variant="secondary">add a card</Button>
          <Button onClick={handletoogle} variant="light">cancle</Button>{' '}
        </Modal.Footer>
      </Modal.Dialog>
    </div>
  )
}
