import React, { useState } from 'react'
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';


export default function DisplayCards(props) {
    const {handleCard, handleToogle} = props
    const [cardName, setCardName] = useState("") 
    const handleCardName = (event) => {
        setCardName(event.target.value)        
    }

    return (   
        <div
            className="modal show"
            style={{ display: 'block', position: 'initial' }}>            
            <Modal.Dialog >
                <Modal.Body  >
                    <input onChange={handleCardName} style={{ width: "100%" }} type="text" name="boardTitle" id="title" />
                </Modal.Body>
                <Modal.Footer className='d-flex justify-content-between'>
                    <Button onClick={()=>handleCard(cardName)} id='title' variant="primary">add a card</Button>
                    <button onClick={handleToogle}>X</button>
                </Modal.Footer>
            </Modal.Dialog>            
        </div>       
       
    )
}
