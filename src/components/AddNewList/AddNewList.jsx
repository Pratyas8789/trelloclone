import React, { useState } from 'react'
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

export default function AddNewList(props) {
  const { handleList, handleToogle } = props
  const [name, setName] = useState("")

  const handleName = (event) => {
    setName(event.target.value)
  }
  return (
    <div
      className="modal show"
      style={{ display: 'block', position: 'initial' }}
    >
      <Modal.Dialog>
        <Modal.Body>
          <input onChange={handleName} type="text" name="boardTitle" id="title" />
        </Modal.Body>
        <Modal.Footer className='d-flex justify-content-between' >
          <Button onClick={() => handleList(name)} type='submit' variant="light">add a card</Button>
          <button onClick={handleToogle} ><i class="fa fa-times fa-lg " style={{outline:"none"}} aria-hidden="true"></i></button>
        </Modal.Footer>
      </Modal.Dialog>
    </div>
  );
}
