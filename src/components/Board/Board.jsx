import React, { useState, useEffect } from 'react'
import Form from '../Form/Form';
import { deleteBoard, getboard } from '../Services/Services'
import { Link } from 'react-router-dom'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

export default function Board() {
    let allBoards;
    const [toogle, setToogle] = useState(false)
    const [board, setBoard] = useState('')
    useEffect(() => {
        getboard()
            .then((response) => {
                setBoard(response)
            })
    }, [])
    const handleDelete = (id) => {
        deleteBoard(id)
            .then(() => {
                setBoard((preBoard) => {
                    const obj = {}
                    const filteredBoard = (preBoard.data).filter(data => data.id !== id)
                    obj["data"] = [...filteredBoard]
                    return obj
                })
            })
    }
    const handleToogle = () => {
        setToogle(!toogle)
    }

    const handleBoard = (data) => {
        setBoard(data)
    }

    if (board !== "") {
        allBoards = (board.data).map((card) => {
            return (
                <Card key={card.id} className='m-4' style={{ width: '18rem', backgroundColor:"#0B5B8F" }}>
                    <Link to={card.id}>
                        <Card.Body style={{color:"white"}}>
                            <Card.Title>{card.name}</Card.Title>
                        </Card.Body>
                    </Link>
                    <Button onClick={() => handleDelete(card.id)} variant="danger">delete <i class="fa fa-trash" aria-hidden="true"></i></Button>
                </Card>
            )
        })
    }

    return (
        <>
            {board !== "" && board !== undefined &&
                <div className='boardContainer d-flex flex-wrap'>
                    {allBoards}
                    <div onClick={handleToogle} className="card m-3 d-flex justify-content-center align-items-center" style={{ width: "18rem", height: "7rem" }}>
                        <h5>Create new board</h5>
                        <p>{10-board.data.length} remaining</p>
                    </div>
                    {toogle && <Form handleBoard={handleBoard} handleToogle={handleToogle} />}
                </div>
            }
        </>
    )
}
