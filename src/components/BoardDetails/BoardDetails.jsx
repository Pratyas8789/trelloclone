import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { deletelist, getlist, postlist, getCards } from '../Services/Services'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import AddNewList from '../AddNewList/AddNewList';
import GetAllCards from '../GetAllCards/GetAllCards.jsx';

export default function BoardDetails() {
  const { id } = useParams()
  const [list, setList] = useState([])
  const [toogle, setToggle] = useState(false)

  const handleToogle = () => {
    setToggle(!toogle)
  }

  useEffect(() => {
    getlist(id)
      .then((responce) => {
        setList(responce.data)
      })
  }, [])

  const deleteListItems = (id) => {
    deletelist(id)
      .then(() => {
        setList((preList) => {
          const filteredList = (preList).filter(list => list.id !== id)
          return filteredList
        })
      })
  }

  const handleList = (name) => {
    postlist(name, id)
      .then(() => {
        const data = getlist(id)
        return data
      })
      .then((data) => {
        setList(data.data)
      })
  }
  let allList;
  if (list !== "" && list !== undefined) {
    allList = list.map((listItems, index) => {
      return (
        <Card key={listItems.id} className='m-4 ' style={{minWidth: '18rem',height:"fit-content"}}>
          <Card.Body>
            <div className='d-flex justify-content-between'>
              <Card.Title>{listItems.name}</Card.Title>
              <Button onClick={() => deleteListItems(listItems.id)} variant="light">...</Button>{' '}
            </div>
            <GetAllCards id={listItems.id} />
          </Card.Body>
        </Card>
      )
    })
  }
  return (
    <div className=' d-flex ' style={{width:"100%", height:"90%",overflowX:"scroll", backgroundColor:"#0279BF"}}>
      {allList}
      <Card className='m-4' style={{minWidth: '18rem',height:"fit-content"}}>
        <Card.Body onClick={handleToogle} style={{backgroundColor:"#3086C4", color:"white" }} className='d-flex justify-content-center align-items-center'>
          <h5>+ add another list</h5>
        </Card.Body>
        {toogle && <AddNewList handleToogle={handleToogle} handleList={handleList} />}
      </Card>
    </div>
  )}