import React from 'react'
import { getboard, postboard } from '../Services/Services'

import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

export default function Form(props) {

  const { handleBoard, handleToogle } = props

  const handleSubmit = (event) => {
    event.preventDefault()
    handleToogle()
    postboard(event.target.title.value)
      .then(() => {
        const data = getboard()
        return data
      })
      .then((data) => {
        handleBoard(data)
      })
  }
  return (
    <form onSubmit={handleSubmit}>
      <div
        className="modal show"
        style={{ display: 'block', position: 'absolute' }}
      >
        <Modal.Dialog>
          <Modal.Header onClick={props.handleToogle} closeButton>
            <Modal.Title>Create board</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h3>board title</h3>
            <input type="text" name="boardTitle" id="title" />
          </Modal.Body>
          <Modal.Body>
            <select name="" id="">
              <option value="">private</option>
              <option value="">workspace</option>
              <option value="">public</option>
            </select>
          </Modal.Body>
          <Modal.Footer>
            <Button type='submit' variant="primary">Start with a template</Button>
          </Modal.Footer>
        </Modal.Dialog>
      </div>
    </form>
  )
}
