import React, { useEffect, useState } from 'react'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { getCards, deleteCard, postCard } from '../Services/Services'
import AddNewCard from '../AddNewCard/AddNewCard';
import ToDos from '../ToDos/ToDos';

export default function GetAllCards(props) {
  const { id } = props
  const [cards, setCards] = useState("")
  const [toogle, setToogle] = useState(false)
  const [listId, setListId] = useState("")
  const [show, setShow] = useState(false);

  let getAllCards;
  useEffect(() => {
    getCards(id)
      .then((response) => {
        setCards(response)
      })
  }, [])
  const handleDeleteCard = (id) => {
    deleteCard(id)
      .then(() => {
        setCards((preCards) => {
          const obj = {}
          const filteredCard = (preCards.data).filter(data => data.id !== id)
          obj["data"] = [...filteredCard]
          return obj
        })
      })
  }

  const handleToogle = () => {
    setToogle(!toogle)
  }

  const handleCard = (name) => {
    postCard(name, id)
      .then(() => {
        const data = getCards(id)
        return data
      })
      .then((data) => {
        setCards(data)
      })
  }

  const handleShow = (id) => {
    setShow(!show)
    setListId(id)
  };

  if (cards !== "") {
    getAllCards = (cards.data).map((card) => {
      return (
        <Card key={card.id} className='d-flex justify-content-between m-2' style={{ width: '14rem' }}>
          <div className='d-flex justify-content-between' >
          <Button variant="light" onClick={() => handleShow(card.id)}>
            {card.name}
          </Button>
          <Button onClick={() => handleDeleteCard(card.id)} variant="light"><i class="fa fa-archive" aria-hidden="true"></i></Button>{' '}         
          </div>
          {show && <ToDos id={listId} handleShow={handleShow} />}
        </Card>
      )
    })
  }
  return (
    <>
      {cards !== "" && getAllCards}
      <Button onClick={handleToogle} id='title' variant="light">+ Add a card</Button>
      {toogle && <AddNewCard handleToogle={handleToogle} handleCard={handleCard} />}
    </>
  )
}
