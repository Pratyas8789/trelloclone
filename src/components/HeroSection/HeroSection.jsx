import React, { useEffect, useState } from 'react'
import Board from '../Board/Board'

export default function HeroSection() {

    return (
        <div className='HeroSection' >
            <h3>Your WorkSpace</h3>
            <h4>Trello Workspace</h4>
            <div className="cardContainer">
                <Board />
            </div>
        </div>
    )
}   