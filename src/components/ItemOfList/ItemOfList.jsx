import React, { useState } from 'react'
import { useEffect } from 'react'
import { getCheckItems, deleteCheckItems, postCheckItems, updateCheckItems } from '../Services/Services'
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import AddItem from '../AddItem/AddItem';


export default function ItemOfList(props) {
    const { id, cardId } = props
    const [item, setItem] = useState("")

    const [toogle, setToogle] = useState(false)

    useEffect(() => {
        getCheckItems(id)
            .then((response) => {
                setItem(response)
            })
    })


    const handleDelete = (itemId) => {
        deleteCheckItems(id, itemId)
            .then(() => {
                setItem((preItems) => {
                    const obj = {}
                    const filteredItem = (preItems.data).filter(data => data.id !== itemId)
                    obj["data"] = [...filteredItem]
                    return obj
                })
            })
    }

    const handletoogle = () => {
        setToogle(!toogle)
    }

    const handleItemName = (name) => {
        postCheckItems(name, id)
            .then(() => {
                const data = getCheckItems(id)
                return data
            })
            .then((data) => {
                setItem(data)
            })
    }
    const handleStatus = (itemId, status) => {
        updateCheckItems(cardId, itemId, status)
            .then(() => {
                setItem((preItem) => {
                    const obj = {}
                    const updateItem = (preItem.data).map((item) => {
                        if (item.id == itemId) {
                            item.state = status
                        }
                        return item
                    })
                    obj["data"] = [...updateItem]
                    return obj

                })
            })
    }

    let getAllItem;

    if (item !== "") {
        getAllItem = (item.data).map((item) => {
            return (
                <Card key={item.id} className=' m-2' style={{ width: '100%' }}>
                    <div className='d-flex justify-content-between'>
                        <div className='d-flex align-items-center' >
                            {item.state == "complete" && <input className='m-2' onClick={() => handleStatus(item.id, "incomplete")} type="checkbox" name="" id="" checked />}
                            {item.state !== "complete" && <input className='m-2' onClick={() => handleStatus(item.id, "complete")} type="checkbox" name="" id="" />}
                            {item.name}
                        </div>
                        <Button onClick={() => handleDelete(item.id)} style={{ width: "5rem" }} variant="light"><i class="fa fa-trash-o" aria-hidden="true"></i></Button>{' '}
                    </div>
                </Card>
            )
        })
    }

    return (
        <>
            {item !== "" && getAllItem}
            {toogle && <AddItem handletoogle={handletoogle} handleItemName={handleItemName} />}
            <br />
            <Button onClick={handletoogle} variant="secondary">+Add an item</Button>{' '}
        </>
    )
}
