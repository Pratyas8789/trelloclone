import React from 'react'
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
export default function NavBar() {
    return (      
        <Navbar expand="lg"  bg="primary" variant="dark">
            <Container variant="primary" className='d-flex justify-content-between m-0' style={{height:"10%"}} >
                <Link to="/" >
                <Button variant="success">Home</Button>
                </Link>   
                <h1>Trello</h1>        
                <input type="text" name="" id="" />
            </Container>
        </Navbar>
    )
}
