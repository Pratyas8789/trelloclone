import axios from 'axios'

const key = "ff2eceff47972f0b75a008ccc19ce860"
const token = "ATTA49c958b26b7c402bf63ea130fef33ee62aab4cda7890ddc5e4ae28397d54401c7C37BC32"

export function getboard() {
  const boardsData = axios
    .get(`https://api.trello.com/1/members/me/boards?key=${key}&token=${token}`)
    .catch(err => console.log(err))
  return boardsData
}

export function getlist(id) {
  const boardsData = axios
    .get(`https://api.trello.com/1/boards/${id}/lists?key=${key}&token=${token}`)
    .catch(err => console.log(err))
  return boardsData
}

export function postboard(title) {
  const updatedData = axios
    .post(`https://api.trello.com/1/boards/?name=${title}&key=${key}&token=${token}`)
    .catch(err => console.log(err))
  return updatedData
}

export function postlist(name, id) {
  const updatedList = axios
    .post(`https://api.trello.com/1/boards/${id}/lists?name=${name}&key=${key}&token=${token}`)
    .catch(err => console.log(err))
  return updatedList
}

export function deleteBoard(id) {
  return axios
    .delete(`https://api.trello.com/1/boards/${id}?key=${key}&token=${token}`)
}

export function deletelist(id) {
  return axios
    .put(`https://api.trello.com/1/lists/${id}/closed?value=true&key=${key}&token=${token}`)
}

export function postCard(name,id) {
  return axios
    .post(`https://api.trello.com/1/cards?name=${name}&idList=${id}&key=${key}&token=${token}`)
}

export function getCards(id) {
  return axios
    .get(`https://api.trello.com/1/lists/${id}/cards?key=${key}&token=${token}`)
}

export function deleteCard(id) {
  return axios
    .delete(`https://api.trello.com/1/cards/${id}?key=${key}&token=${token}`)
}

export function getChecklist(id) {
  const boardsData = axios
    .get(`https://api.trello.com/1/cards/${id}/checklists?key=${key}&token=${token}`)
    .catch(err => console.log(err))
  return boardsData
}

export function postCheckList(name, id) {
  const updatedList = axios
    .post(`https://api.trello.com/1/checklists?name=${name}&idCard=${id}&key=${key}&token=${token}`)
    .catch(err => console.log(err))
  return updatedList
}

export function deleteChecklist(id1,id2) {
  const boardsData = axios
    .delete(`https://api.trello.com/1/cards/${id1}/checklists/${id2}?key=${key}&token=${token}`)
    .catch(err => console.log(err))
  return boardsData
}

export function getCheckItems(id) {
  return axios
    .get(`https://api.trello.com/1/checklists/${id}/checkItems?key=${key}&token=${token}`)
}

export function deleteCheckItems(id1,id2) {
  return axios
    .delete(`https://api.trello.com/1/checklists/${id1}/checkItems/${id2}?key=${key}&token=${token}`)
}

export function postCheckItems(name, id) {
  const updatedList = axios
    .post(`https://api.trello.com/1/checklists/${id}/checkItems?name=${name}&key=${key}&token=${token}`)
    .catch(err => console.log(err))
  return updatedList
}

export function updateCheckItems(cardId, id,state){
  const updateditems = axios
    .put(`https://api.trello.com/1/cards/${cardId}/checkItem/${id}?state=${state}&key=${key}&token=${token}`)
    .catch(err => console.log(err))
  return updateditems
}