import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { getChecklist, postCheckList, deleteChecklist, postCheckItems } from '../Services/Services'
import Card from 'react-bootstrap/Card';
import AddCheckList from '../AddCheckList/AddCheckList';
import ItemOfList from '../ItemOfList/ItemOfList';
export default function ToDos(props) {

    const { handleShow, id } = props
    const show = true;

    const [checkList, setCheckList] = useState("")
    const [toogle, setToogle] = useState("")

    useEffect(() => {
        getChecklist(id)
            .then((response) => {
                setCheckList(response)
            })
    }, [])

    const handleCard = (name) => {
        postCheckList(name, id)
            .then(() => {
                const data = getChecklist(id)
                return data
            })
            .then((data) => {
                setCheckList(data)
            })

    }

    const handleToogle = () => {
        setToogle(!toogle)
    }

    const handleDelete = (listid) => {
        deleteChecklist(id, listid)
            .then(() => {
                setCheckList((preList) => {
                    const obj = {}
                    const filteredCard = (preList.data).filter(data => data.id !== listid)
                    obj["data"] = [...filteredCard]
                    return obj
                })
            })

    }

    let allListItems;

    if (checkList !== "") {
        allListItems = (checkList.data).map((list) => {
            return (
                <>
                <Card key={list.id} className='m-2' style={{ width: '100%' }}>
                    <Card.Body className='d-flex flex-column align-items-between'>
                        <Card.Title className='d-flex justify-content-end' >
                            <Button onClick={() => handleDelete(list.id)} variant="light">delete</Button>{' '}
                        </Card.Title>
                        <Card.Text>
                            {list.name}
                            <ItemOfList id={list.id} cardId={id} />
                        </Card.Text>
                    </Card.Body>
                </Card>
                </>
            )
        })
    }


    return (
        <>
            {checkList !== "" &&
                <Modal show={show} onHide={handleShow} animation={false}>
                    <Modal.Header closeButton>
                        <Modal.Title>ToDos </Modal.Title>
                    </Modal.Header>
                    <Modal.Body className='d-flex justify-content-end'>
                        <Button onClick={handleToogle} variant="light">CheckList</Button>{' '}
                    </Modal.Body>
                    <Modal.Body>
                        {allListItems}
                        {toogle && <AddCheckList handleToogle={handleToogle} handleCard={handleCard} />}
                    </Modal.Body>
                    <Modal.Footer>
                    </Modal.Footer>
                </Modal>}
        </>
    )
}
